﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bookstore.Models;

namespace Bookstore.Controllers
{
    public class StoreController : Controller
    {
        BookstoreEntities storeDB= new BookstoreEntities();
        public ActionResult Index()
        {
            var genres = storeDB.Genres.ToList();
            return View(genres);
        }
        public ActionResult Browse(string genre)
        {
            var genreModel = storeDB.Genres.Include("Books")
                .Single(g => g.Name == genre);

            return View(genreModel);
        }
        public ActionResult Search(string searchString)
        {
            var books = from bk in storeDB.Books select bk;
            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(gen => gen.Title.Contains(searchString));
            }

            return View(books);
        }
        public ActionResult Details(int id)
        {
            var book = storeDB.Books.Find(id);
            return View(book);
        }
        // GET: /Store/GenreMenu
        [ChildActionOnly]
        public ActionResult GenreMenu()
        {
            var genres = storeDB.Genres.ToList();
            return PartialView(genres);
        }
    }
}