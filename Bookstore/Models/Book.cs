﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Bookstore.Models
{
    [Bind(Exclude="BookId")]
    public class Book
    {
        [ScaffoldColumn(false)] public int BookId { get; set; } 
        [Display(Name="Tür")] public int GenreId { get; set; }
        [Display(Name="Yazar")] public int WriterId { get; set; }
        [Required(ErrorMessage = "Kitap ismi zorunludur.")]
        [StringLength(160)] public string Title { get; set; }
        [Required(ErrorMessage="Fiyat girilmesi zorunludur")]
        [Display(Name="Fiyat")]
        [Range(0.01,100.00,ErrorMessage="Fiyat 0.01 ve 100.00 arasında olmalıdır")] public decimal Price { get; set; }
        [Display(Name="Kitap Kapağı Adresi"),StringLength(1024)]
        public string BookcoverUrl { get; set; }
        public virtual Genre Genre { get; set; } 
        public virtual Writer Writer { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; } 
    }
}