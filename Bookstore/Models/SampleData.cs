﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Bookstore.Models
{
    public class SampleData : DropCreateDatabaseIfModelChanges<BookstoreEntities>
    {
        protected override void Seed(BookstoreEntities DbContext)
        {
            var genres = new List<Genre>
            {
                new Genre { Name = "Fantasy" },
                new Genre { Name = "Sci-Fi" },
                new Genre { Name = "Romance" },
                new Genre { Name = "Classic" },
                new Genre { Name = "Horror" },
                new Genre { Name = "Fable" }
            };

            var writers = new List<Writer>
            {
                new Writer { Name = "Stephen King" },
                new Writer { Name = "J.K Rowling" },
                new Writer { Name = "George R.R. Martin" },
                new Writer { Name = "Stephenie Meyer" },
                new Writer { Name = "Charles Dickens" },
                new Writer { Name = "La Fontaine" },
                new Writer { Name="Orson Scott Card" }
            };
            new List<Book>
            {
                new Book { Title = "Under The Dome", Genre = genres.Single(g => g.Name == "Horror"), Price = 8.99M, Writer = writers.Single(a => a.Name == "Stephen King"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "The Ant and The Grasshopper", Genre = genres.Single(g => g.Name == "Fable"), Price = 8.99M, Writer = writers.Single(a => a.Name == "La Fontaine"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "Twilight", Genre = genres.Single(g => g.Name == "Romance"), Price = 8.99M, Writer = writers.Single(a => a.Name == "Stephenie Meyer"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "Harry Potter and the Chamber of Secrets", Genre = genres.Single(g => g.Name == "Fantasy"), Price = 8.99M, Writer = writers.Single(a => a.Name == "J.K Rowling"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "Ender's Game", Genre = genres.Single(g => g.Name == "Sci-Fi"), Price = 8.99M, Writer = writers.Single(a => a.Name == "Orson Scott Card"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "The Tale Two Cities", Genre = genres.Single(g => g.Name == "Classic"), Price = 8.99M, Writer = writers.Single(a => a.Name == "Charles Dickens"), BookcoverUrl = "/Content/Images/placeholder.gif" },
                new Book { Title = "Clash of The Kings", Genre = genres.Single(g => g.Name == "Fantasy"), Price = 8.99M, Writer = writers.Single(a => a.Name == "George R.R. Martin"), BookcoverUrl = "/Content/Images/placeholder.gif" },
            }.ForEach(a => DbContext.Books.Add(a));
        }
    }
}