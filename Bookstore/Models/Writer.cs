﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models
{
    public class Writer
    {
        public int WriterId { get; set; }
        public string Name { get; set; }
    }
}